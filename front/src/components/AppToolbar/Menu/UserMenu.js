import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {Avatar, IconButton, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {Link} from "react-router-dom";
import {apiURL} from "../../../config";
import {logoutRequest} from "../../../store/actions/usersActions";


const useStyles = makeStyles(theme => ({
    avatar: {
        width: theme.spacing(4),
        height: theme.spacing(4)
    }
}));

const UserMenu = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <IconButton
                onClick={handleClick}
                color="inherit"
            >
                {user.avatar ?
                    <Avatar
                        src={apiURL + '/' + user.avatar}
                        className={classes.avatar}
                    />
                    :
                    <Avatar className={classes.avatar}/>
                }
            </IconButton>
            <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem disabled>Hello! {user.displayName}</MenuItem>
                <MenuItem>
                    <Link to="/addNewPhoto">Add New Photo</Link>
                </MenuItem>
                <MenuItem>
                    <Link to="/myPhoto">My Photo</Link>
                </MenuItem>
                <MenuItem onClick={() => dispatch(logoutRequest())}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;