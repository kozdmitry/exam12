import React from "react";
import { Button, makeStyles, Grid } from "@material-ui/core";
import Backdrop from "../BackDrop/BackDrop";

const useStyles = makeStyles({
    Modal: {
        position: "fixed",
        zIndex: "500",
        backgroundColor: "whitesmoke",
        width: "55%",
        border: "1px solid #ccc",
        padding: "10px",
        left: "15%",
        top: "7%",
        boxSizing: "border-box",
        transition: "all 0.3s ease-out",
    },
    media: {
        height: "350px",
        width: "500px",
    },
});

const Modal = (props) => {
    const classes = useStyles();

    return (
        <>
            <Backdrop show={props.show} onClick={props.closed} />
            <Grid
                container
                alignItems="center"
                direction="column"
                className={classes.Modal}
                style={{
                    transform: props.show ? "translateY(0)" : "translateY(-100vh)",
                    opacity: props.show ? "1" : "0",
                }}
            >
                <p>{props.title}</p>
                <img src={props.image} alt={props.image} className={classes.media} />
                <Button onClick={props.closed}>Close</Button>
            </Grid>
        </>
    );
};

export default Modal;
