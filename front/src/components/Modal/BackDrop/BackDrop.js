import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
    Backdrop: {
        width: "100%",
        height: "100%",
        position: "fixed",
        zIndex: 100,
        left: 0,
        top: 0,
        backgroundColor: "rgba(0, 0, 0, 0.5)",
    },
});

const Backdrop = ({ show, onClick }) => {
    const classes = useStyles();
    return show ? <div className={classes.Backdrop} onClick={onClick} /> : null;
};

export default Backdrop;
