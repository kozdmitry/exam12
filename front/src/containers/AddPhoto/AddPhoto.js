import {useState} from "react";
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";
import FormElement from "../../components/Form/FormElement";
import FileInput from "../../components/Form/FileInput";
import {Button} from "@material-ui/core";
import {postPhotosRequest} from "../../store/actions/photosActions";

const AddPhoto = () => {
    const dispatch = useDispatch();
    const [photo, setPhoto] = useState({
        title: "",
        image: "",
    });

    const inputChangeHandler = (e) => {
        const { name, value } = e.target;

        setPhoto((prev) => ({ ...prev, [name]: value }));
    };

    const submitFormHandler = (e) => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(photo).forEach((key) => {
            formData.append(key, photo[key]);
        });

        dispatch(postPhotosRequest(formData))
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPhoto((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    return (
        <>
            <Grid container spacing={1} direction="column" component="form" onSubmit={submitFormHandler}>
                <Grid item xs>
                    <FormElement
                        label="Title"
                        type="text"
                        onChange={inputChangeHandler}
                        name="title"
                        value={photo.title}
                    />
                </Grid>
                <Grid item>
                    <FileInput name="image" label="Image" onChange={fileChangeHandler} />
                </Grid>
                <Grid item xs>
                    <Button variant="contained" type="submit" color="primary">
                        Add Photo
                    </Button>
                </Grid>
            </Grid>
        </>
    );
};

export default AddPhoto;