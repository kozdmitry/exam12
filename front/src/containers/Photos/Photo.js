import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import {Card, CardActions, CardHeader, CardMedia, IconButton} from "@material-ui/core";
import Modal from "../../components/Modal/Modal/Modal";
import {Link} from "react-router-dom";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    }
});



const Photo = ({title,image, id, author}) => {
    const classes = useStyles();

    const [modal, setModal] = useState(false);

    const openModal = () => {
        setModal(true);
    };

    const closeModal = () => {
        setModal(false);
    };

    return (
        <Grid item xs sm md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={apiURL + "/" + image}
                    title={title}
                    className={classes.media}
                    onClick={openModal}
                />
                <CardActions>
                    <IconButton component={Link} to={'/user/' + id}>
                    </IconButton>
                </CardActions>
            </Card>
            <Modal show={modal} closed={closeModal} image={apiURL + "/" + image} title={title} />
        </Grid>
    );
};

export default Photo;