import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotosRequest} from "../../store/actions/photosActions";
import Grid from "@material-ui/core/Grid";
import Photo from "./Photo";

const Photos = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPhotosRequest());
    }, [dispatch]);

    const photos = useSelector((state) => state.photos.photos);
    return (
        <Grid container spacing={2}>
            {photos.map((photo) => (
                <Photo
                    key={photo._id} image={photo.image} title={photo.title} author={photo.user} />
            ))}
        </Grid>
    );

};

export default Photos;