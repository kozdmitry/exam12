import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "./containers/Login/Login";
import Layout from "./components/Layout/Layout";
import Register from "./containers/Register/Register";
import AddPhoto from "./containers/AddPhoto/AddPhoto";
import Photos from "./containers/Photos/Photos";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/addNewPhoto" component={AddPhoto} />
            <Route path="/" exact component={Photos} />
        </Switch>
    </Layout>
);

export default App;
