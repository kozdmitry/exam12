import { createSlice } from "@reduxjs/toolkit";

const name = "photos"

const photosSlice = createSlice ({
        name,
        initialState: {
                photos: [],
                photosLoading: false,
                photosError: null
        },
        reducers: {
            fetchPhotosRequest: (state) => {
                state.photosLoading = true;
            },
            fetchPhotosSuccess: (state, { payload: photos }) => {
                state.photosLoading = false;
                state.photos = photos;
            },
            fetchPhotosFailure: (state) => {
                state.photosError = false;
            },
            postPhotosRequest: (state) => {
                state.photosLoading = true;
            },
            postPhotosSuccess: (state) => {
                state.photosLoading = false;
            },
            postPhotosFailure: (state) => {
                state.photosError = false;
            },
    },
});

export default photosSlice;