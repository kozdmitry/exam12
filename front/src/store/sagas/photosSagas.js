import { put, takeEvery } from "redux-saga/effects";
import {
    fetchPhotosFailure,
    fetchPhotosRequest,
    fetchPhotosSuccess, postPhotosFailure, postPhotosRequest, postPhotosSuccess
} from "../actions/photosActions";
import axiosApi from "../../axiosApi";
import { addNotification } from "../actions/notifierActions";
import {historyPush} from "../actions/historyActions";

export function* fetchPhotos({ payload: userId }) {
    try {
        let url = "/photos";

        if (userId) {
            url += "?user=" + userId;
        }

        const response = yield axiosApi.get(url);
        yield put(fetchPhotosSuccess(response.data));
    } catch (e) {
        yield put(fetchPhotosFailure());
        yield put(addNotification({ message: "Fetch photos failed", options: { variant: "error" } }));
    }
}

export function* postPhotos ({payload: photoData}){
    try {
        yield axiosApi.post("/photos", photoData);
        yield put(postPhotosSuccess());
        yield put(
            addNotification({ message: "Your Photo Successfully Added ", options: { variant: "success" } })
        );
        yield put(historyPush("/"));
    } catch (e) {
        yield put(postPhotosFailure());
        yield put(addNotification({ message: "Your Photo has not been added", options: { variant: "error" } }));
    }
}


const photosSagas = [
    takeEvery(fetchPhotosRequest, fetchPhotos),
    takeEvery(postPhotosRequest, postPhotos),
];

export default photosSagas;