import photosSlice from "../slices/photosSlice";

export const {
    fetchPhotosRequest,
    fetchPhotosSuccess,
    fetchPhotosFailure,
    postPhotosRequest,
    postPhotosSuccess,
    postPhotosFailure,
    deletePhotosRequest,
    deletePhotosSuccess,
    deletePhotosFailure,

} = photosSlice.actions;