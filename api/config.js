const path = require("path");
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public"),
    db: {
        url: "mongodb://localhost/exam12",
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        },
    },
    facebook: {
        appId: "4315221515168350",
        appSecret: "098708e8b9b9b3f0e37a789977de142e",
    },
};
