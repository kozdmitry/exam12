const mongoose = require('mongoose');
const config = require("./config");
const Photo = require("./models/Photo");
const User = require("./models/User");
const {nanoid} = require("nanoid");


const run = async () => {

    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    };

    const [test1, test2, test3] = await User.create(
        {
            displayName: "Vasya",
            password: '12345',
            token: nanoid(),
            email: "test1@test.ru"

    },
        {
            displayName: "Andrew",
            password: '12345',
            token: nanoid(),
            email: "test2@test.ru"

    },
        {
            displayName: "Vitas",
            password: '12345',
            token: nanoid(),
            email: "test3@test.ru"

    });

    await Photo.create(
        {
            title: "BMW X5M",
            user: test1,
            image: "fixtures/x5m.jpg",
        },
        {
            title: "BMW X5M",
            user: test1,
            image: "fixtures/m5.jpg",
        },
        {
            title: "BMW X5M",
            user: test1,
            image: "fixtures/m8.jpg",
        },
        {
            title: "ML63AMG",
            user: test2,
            image: "fixtures/ml63.jpg",
        },
        {
            title: "G63",
            user: test1,
            image: "fixtures/g63.jpg",
        },
        {
            title: "E63AMG",
            user: test1,
            image: "fixtures/e63amg.jpg",
        },
        {
            title: "Audi RS6",
            user: test3,
            image: "fixtures/rs6.jpg",
        },
        {
            title: "Audi Q7",
            user: test3,
            image: "fixtures/q7.jpg",
        },
        {
            title: "Audi R8",
            user: test3,
            image: "fixtures/r8.jpg",
        },
    );

    await mongoose.connection.close();
};

run().catch(console.error);