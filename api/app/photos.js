const express = require("express");
const auth = require("../middleware/auth");
const upload = require("../multer").photos;
const Photo = require("../models/Photo");
const router = express.Router();

router.get("/", async (req, res) => {
    try {
        if (req.query.user) {
            const userPhotos = await Photo.find({ user: req.query.user }).populate("user", "displayName");
            return res.send(userPhotos);
        }

        const photos = await Photo.find().populate("user", "displayName");
        return res.send(photos);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
    try {
        const photoData = {
            title: req.body.title,
            image: req.file.filename,
            user: req.user,
        };

        const photo = new Photo(photoData);

        await photo.save();
        return res.send(photo);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.delete("/:id", auth, async (req, res) => {
    try {
        await Photo.deleteOne({ _id: req.params.id });
        return res.send("Deleted");
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;
